from handler import *

h = AdHandler()

dt = datetime.utcnow()

print(h.handle(Post('author1', 'blog1', 'post1', dt, 'hash1')))
print(h.author_posts)
print(h.handle(Post('author1', 'blog1', 'post1', dt - timedelta(minutes=1), 'hash1')))
print(h.author_posts)