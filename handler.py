from common import *


class CachedPost:
    def __init__(self, blog_url_hash, blogans_body_hash, create_date):
        self.blog_url_hash = blog_url_hash
        self.blogans_body_hash = blogans_body_hash
        self.create_date = create_date


class Post:
    def __init__(self, author_url, blog_url, url, create_date, blogans_body_hash):
        self.author_url = author_url
        self.blog_url = blog_url
        self.url = url
        self.create_date = create_date
        self.blogans_body_hash = blogans_body_hash


# получаем кэшпост из поста
def get_cache_post(post):
    return CachedPost(blog_url_hash=get_hash(post.blog_url),
                      blogans_body_hash=post.blogans_body_hash,
                      create_date=post.create_date)


class AdHandler:
    def __init__(self):
        self.author_posts = dict()

    # обновляем посты автора в кэше, удаляя старые
    def refresh_posts(self, author_url_hash):
        self.author_posts[author_url_hash] = [cp for cp in self.author_posts[author_url_hash] if is_fresh(cp)]

    # возвращаем результат, является пост рекламой или нет, true = реклама, false = нет
    def handle(self, post):
        author_url_hash = get_hash(post.author_url)

        if author_url_hash not in self.author_posts.keys():
            if is_fresh(post):
                self.author_posts[author_url_hash] = [get_cache_post(post)]
            return False
        else:
            self.refresh_posts(author_url_hash)
            blog_url_hash = get_hash(post.blog_url)

            # если пост свежий, и таких же (ключ это урл+боди+dt) больше нет, то записываем
            if is_fresh(post) and \
                    len([cp for cp in self.author_posts[author_url_hash]
                         if cp.blog_url_hash == blog_url_hash and
                            cp.blogans_body_hash == post.blogans_body_hash and
                            cp.create_date == post.create_date]) == 0:
                self.author_posts[author_url_hash].append(get_cache_post(post))

            # проверяем, есть ли в текущем посте с другим blog_url и одинаковым бади, если есть это реклама!!
            if len([cp for cp in self.author_posts[author_url_hash]
                    if blog_url_hash != cp.blog_url_hash and post.blogans_body_hash == cp.blogans_body_hash]) > 0:
                return True
            else:
                if len(self.author_posts[author_url_hash]) == 0:
                    self.author_posts.pop(author_url_hash)
                return False
