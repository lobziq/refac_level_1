import hashlib
from datetime import datetime, timedelta


def get_hash(value):
    return hashlib.md5(value.encode()).hexdigest()


def is_fresh(post):
    return post.create_date > datetime.utcnow() - timedelta(days=1)
